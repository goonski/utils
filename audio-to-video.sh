#!/bin/bash
# convert an audio file to a video, with static image if embedded in audio file
#
# to use this e.g. in a folder of many audio files, use the following command
# for FILE in *.mp3; do /path/to/audio-to-video.sh "$FILE"; done
#
# requires: ffmpeg
# argument 1: name of audio file

FILE=$1

if [ ! -f "$FILE" ]; then
    echo "$FILE does not exist"
    exit
fi

IMAGE_FILE="${FILE%.*}.jpg"
VIDEO_FILE="${FILE%.*}.mp4"

# get image out of audio file
ffmpeg \
    -i "$FILE" \
    -an -c:v copy \
    -y \
    "$IMAGE_FILE"

ffmpeg \
    -loop 1 \
    -i "$IMAGE_FILE" -i "$FILE" \
    -vf "scale=1920:1080:force_original_aspect_ratio=decrease,pad=1920:1080:-1:-1:color=black,setsar=1,format=yuv420p" \
    -shortest \
    -fflags +shortest \
    -y \
    "$VIDEO_FILE"
