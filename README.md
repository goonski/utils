# utils

some tools I use to make things easier

## `sync_keepass`

> NOTE: this depends on [`rclone`](https://rclone.org/)

variables in `.env`
- `KDBX_PATH` path to file you wish to synchronize
- `DRIVE_REMOTE` name of you rclone remote

usage:

`keepass pull`
    pulls kdbx

`keepass push`
    uploads kdbx to drive

## `unzip.sh`

unzips all `.zip` files in a directory, and puts the contents into folders by the basename of the zip file,
except for removing the extension

## `convert-heic-to-jpg.sh`

converts iphone photos (`.HEIC`) to `.jpg` via `heif-convert`

## `instaloader.sh`

archive posts of your favorite instagram accounts via instaloader

requirements:

- python3
- [instaloader](https://github.com/instaloader/instaloader)

## `docker-service.sh`

template script

start, restart and stop functions can be called as arguments after script.
can of course be used for other things than docker containers
