#!/bin/bash

git filter-branch -f --commit-filter '
        if [ "$GIT_COMMITTER_NAME" = youngtrashbag ];
        then
                GIT_COMMITTER_NAME="goonski";
                GIT_AUTHOR_NAME="goonski";
                GIT_COMMITTER_EMAIL="elias.albisser@gmail.com";
                GIT_AUTHOR_EMAIL="elias.albisser@gmail.com";
                git commit-tree "$@";
        else
                git commit-tree "$@";
        fi' HEAD
